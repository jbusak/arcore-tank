﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumveeScale : MonoBehaviour
{
    private float maxX = 0.01f;
    private float maxY = 0.01f;
    private float maxZ = 0.01f;

    protected GameObject slider;

    private void Start()
    {
        slider = GameObject.Find("Slider");
    }

    void Update()
    {
        float percentage = slider.GetComponent<Slider>().value;

        transform.localScale = new Vector3(maxX * percentage, maxY * percentage, maxZ * percentage);
    }
}
