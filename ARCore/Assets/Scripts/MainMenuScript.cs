﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{

    public void ShowTankScene()
    {
        SceneManager.LoadScene("GameSceneTank");
    }

    public void ShowHumveeScene()
    {
        SceneManager.LoadScene("GameSceneHumvee");
    }

    public void ShowRifleScene()
    {
        SceneManager.LoadScene("GameSceneRifle");
    }

    public void ShowScopeScene()
    {
        SceneManager.LoadScene("GameSceneScope");
    }

    public void ShowMerkavaScene()
    {
        SceneManager.LoadScene("ComponentScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackToMain()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Info()
    {
        SceneManager.LoadScene("InfoScene");
    }

}
