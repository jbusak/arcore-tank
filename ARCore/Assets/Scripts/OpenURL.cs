﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURL : MonoBehaviour
{

    public void FERButton()
    {
        Application.OpenURL("https://www.fer.unizg.hr/");
    }

    public void ZTLButton()
    {
        Application.OpenURL("https://tel.fer.unizg.hr/ztel");
    }

}
