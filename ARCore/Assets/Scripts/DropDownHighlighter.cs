﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownHighlighter : MonoBehaviour
{
    public static GameObject tank;

    private Color prevColor;

    private Transform prevComponent;

    private Transform component;

    public void HandleInputData(int val)
    {
        if (val == 0)
        {
            prevComponent.GetComponent<MeshRenderer>().material.SetColor("_Color", prevColor);
            return;
        }

        if (val == 1)
        {
            component = tank.transform.Find("M_carcasa_BUMP");
        }

        if (val == 2)
        {
            component = tank.transform.Find("M_ametralladora");
        }

        if (val == 3)
        {
            component = tank.transform.Find("M_ca_on");
        }
        prevColor = component.GetComponent<MeshRenderer>().material.color;
        component.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);

        if (prevComponent != null)
        {
            prevComponent.GetComponent<MeshRenderer>().material.SetColor("_Color", prevColor);
        }
        prevComponent = component;
    }

}
