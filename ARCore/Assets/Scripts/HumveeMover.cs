﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumveeMover : MonoBehaviour
{
    private Rigidbody rb;
    protected Joystick joystick;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        joystick = FindObjectOfType<Joystick>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = joystick.Horizontal * 0.1f;
        float y = joystick.Vertical * 0.1f;
        Vector3 movement = new Vector3(x, 0, y);
        rb.velocity = movement * 1.5f;
        if (x != 0 && y != 0)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(x, y) * Mathf.Rad2Deg, transform.eulerAngles.z);
        }
    }
}
